package com.example.recyclerview2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val data = mutableListOf<Makanan>(
            Makanan(
                "Kopi",
                "Rp20.000,-",
                "https://www.harapanrakyat.com/wp-content/uploads/2018/10/Kopi-1280x720.jpg"
            ),
            Makanan(
                "Kopi",
                "Rp20.000,-",
                "https://www.harapanrakyat.com/wp-content/uploads/2018/10/Kopi-1280x720.jpg"
            ),
            Makanan(
                "Kopi",
                "Rp20.000,-",
                "https://www.harapanrakyat.com/wp-content/uploads/2018/10/Kopi-1280x720.jpg"
            ),
            Makanan(
                "Kopi",
                "Rp20.000,-",
                "https://www.harapanrakyat.com/wp-content/uploads/2018/10/Kopi-1280x720.jpg"
            ),
            Makanan(
                "Kopi",
                "Rp20.000,-",
                "https://www.harapanrakyat.com/wp-content/uploads/2018/10/Kopi-1280x720.jpg"
            ),
            Makanan(
                "Kopi",
                "Rp20.000,-",
                "https://www.harapanrakyat.com/wp-content/uploads/2018/10/Kopi-1280x720.jpg"
            )
        )

        val adapter = AdapterRecyclerView(data)
        rvMain.layoutManager = GridLayoutManager(baseContext, 3)
        rvMain.adapter = adapter

    }
}
